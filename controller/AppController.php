<?php

/**
 * Class AppController
 */
abstract class AppController
{
    /** @var string|null */
    private $request = null;

    /**
     * AppController constructor.
     */
    public function __construct()
    {
        $this->request = strtolower($_SERVER['REQUEST_METHOD']);
        session_start();
    }

    /**
     * @return bool
     */
    public function isGet(): bool
    {
        return $this->request === 'get';
    }

    /**
     * @return bool
     */
    public function isPost(): bool
    {
        return $this->request === 'post';
    }

    /**
     * @return bool
     */
    public function isAuthorized(): bool {
        if (!isset($_SESSION['id']) || $_SESSION['id'] < 1) {
            return false;
        }

        return true;
    }

    /**
     * @param string|null $fileName
     * @param array $variables
     */
    public function render(string $fileName = null, $variables = []): void
    {
        $view = $fileName ? dirname(__DIR__) . '/view/' . get_class($this) . '/' . $fileName . '.php' : '';

        $output = 'There isn\'t such file to open';

        if (file_exists($view)) {

            extract($variables);

            ob_start();
            include $view;
            $output = ob_get_clean();
        }

        print $output;
    }
}