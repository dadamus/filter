<?php

require_once __DIR__ . '/AppController.php';
require_once __DIR__ . '/../model/UserMapper.php';
require_once __DIR__ . '/../model/FileMapper.php';

/**
 * Class AdminController
 */
class AdminController extends AppController
{
    public function chooseUser(): void
    {
        if (!$this->isAuthorized() || $_SESSION['role'] !== User::ROLE_ADMIN) {
            header('Location: /');
        }

        $userMapper = new UserMapper();
        $this->render('index', [
            'users' => $userMapper->getUsers()
        ]);
    }

    public function userManage(): void
    {
        if (!$this->isAuthorized() || $_SESSION['role'] !== User::ROLE_ADMIN) {
            header('Location: /');
        }

        $userId = $_GET['user_id'];
        $fileMapper = new FileMapper();

        $this->render('list', [
            'files' => $fileMapper->getByUserId($userId),
            'userId' => $userId,
        ]);
    }

    public function adminDelete(): void
    {
        if (!$this->isAuthorized() || $_SESSION['role'] !== User::ROLE_ADMIN) {
            header('Location: /');
        }

        $fileId = $_GET['file_id'];
        $userId = $_GET['user_id'];

        $fileMapper = new FileMapper();
        $fileMapper->delete($fileId, $userId);
        header('Location: /?page=admin_view&user_id=' . $userId);
    }
}