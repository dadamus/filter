<?php

require_once __DIR__ . '/AppController.php';
require_once __DIR__ . '/../model/User.php';
require_once __DIR__ . '/../model/UserMapper.php';
require_once __DIR__ . '/../model/FileMapper.php';

/**
 * Class DefaultController
 */
class DefaultController extends AppController
{
    /**
     * @throws Exception
     */
    public function index(): void
    {
        $fileMapper = new FileMapper();
        $userId = null;

        if ($this->isAuthorized()) {
            $userId = $_SESSION['id'];
        }

        $this->render('index', [
            'isAuthorized' => $this->isAuthorized(),
            'userId' => $userId,
            'files' => $fileMapper->getByUserId($userId)
        ]);
    }

    public function login(): void
    {
        $mapper = new UserMapper();

        $user = null;

        if ($this->isPost()) {

            $user = $mapper->getUser($_POST['email']);

            if (!$user) {
                $this->render('login', ['message' => ['Email not recognized']]);
                return;
            }

            if ($user->getPassword() !== md5($_POST['password'])) {
                $this->render('login', ['message' => ['Wrong password']]);
                return;
            } else {
                $_SESSION['id'] = $user->getId();
                $_SESSION['email'] = $user->getEmail();
                $_SESSION['role'] = $user->getRole();

                $mapper->addUserLoginLog($user->getId());

                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}?page=index");
                exit();
            }
        }


        $this->render('login');
    }
}