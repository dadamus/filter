<?php

require_once __DIR__ . '/AppController.php';
require_once __DIR__ . '/../model/UserMapper.php';


class RegistrationController extends AppController
{
    public function form(): void {
        $message = [];

        try {
            if ($this->isPost()) {
                $mapper = new UserMapper();

                $email = $_POST['email'];
                $name = $_POST['name'];
                $surname = $_POST['surname'];
                $password = $_POST['password'];

                if (strlen($email) < 5) {
                    throw new \Exception('Email musi mieć minimum 5 znaków!');
                }

                if (strlen($name) < 3) {
                    throw new \Exception('Imię musi mieć minimum 3 znaki!');
                }

                if (strlen($surname) < 3) {
                    throw new \Exception('Nazwisko musi mieć minimum 3 znaki!');
                }

                if (strlen($password) < 8) {
                    throw new \Exception('Hasło musi mieć minimum 8 znaków!');
                }

                $mapper->createUser($email, $name, $surname, $password);
                $message[] = 'Użytkownik został zarejestrowany!';
            }
        } catch (\Exception $exception) {
            $message[] = $exception->getMessage();
        }

        $this->render('form', [
            'message' => $message
        ]);
    }
}