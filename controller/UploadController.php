<?php

require_once __DIR__ . '/AppController.php';

/**
 * Class UploadController
 */
class UploadController extends AppController
{
    /**
     * @throws Exception
     */
    public function upload(): void
    {
        if (!$this->isAuthorized()) {
            header('Location: /?page=login');
        }

        $userId = $_SESSION['id'];
        $fileMapper = new FileMapper();

        if ($this->isPost()) {
            $file = $_FILES['to_upload'];

            $extension = strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));

            $fileId = $fileMapper->save($file['name'], $extension, $userId);

            $targetDir = __DIR__ . '/../files/' . $userId . '/';
            $newFileName = $fileId . '.' . $extension;

            if (!file_exists($targetDir)) {
                mkdir($targetDir, 0777, true);
            }

            move_uploaded_file($file['tmp_name'], $targetDir . $newFileName);
        }

        header('Location: /');
    }

    public function delete(): void
    {
        if (!$this->isAuthorized()) {
            header('Location: /?page=login');
        }

        $userId = $_SESSION['id'];
        $fileId = $_GET['file_id'];

        $fileMapper = new FileMapper();

        $fileMapper->delete($fileId, $userId);

        header('Location: /');
    }
}