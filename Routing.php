<?php

require_once __DIR__ . '/controller/DefaultController.php';
require_once __DIR__ . '/controller/RegistrationController.php';
require_once __DIR__ . '/controller/UploadController.php';
require_once __DIR__ . '/controller/AdminController.php';

class Routing
{
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'index' => [
                'controller' => 'DefaultController',
                'action' => 'index'
            ],
            'login' => [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],
            'upload' => [
                'controller' => 'UploadController',
                'action' => 'upload'
            ],
            'delete' => [
                'controller' => 'UploadController',
                'action' => 'delete'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'chooseUser'
            ],
            'admin_view' => [
                'controller' => 'AdminController',
                'action' => 'userManage'
            ],
            'admin_delete' => [
                'controller' => 'AdminController',
                'action' => 'adminDelete'
            ],
            'register' => [
                'controller' => 'RegistrationController',
                'action' => 'form'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page'])
        && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'index';

        if ($this->routes[$page]) {
            $class = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $class;
            $object->$action();
        }
    }
}