<?php

require_once 'User.php';
require_once __DIR__ . '/../Database.php';

/**
 * Class UserMapper
 */
class UserMapper
{
    /**
     * @var Database
     */
    private $database;

    /**
     * UserMapper constructor.
     */
    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * @param string $email
     * @return User
     */
    public function getUser(
        string $email
    ): User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT u.*, ug.group_id
            FROM `user` u
            LEFT JOIN user_group ug ON ug.user_id = u.id 
            WHERE u.email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        return new User($user['id'], $user['name'], $user['surname'], $user['email'], $user['password'], $user['group_id']);

    }

    /**
     * @param string $email
     * @param string $name
     * @param string $surname
     * @param string $password
     * @throws Exception
     */
    public function createUser(string $email, string $name, string $surname, string $password): void
    {
        $connection = $this->database->connect();

        try {
            $password = md5($password);

            $checkEmailQuery = $connection->prepare('SELECT id FROM `user` WHERE email = :email');
            $checkEmailQuery->bindParam(':email', $email, PDO::PARAM_STR);
            $checkEmailQuery->execute();

            $checkEmail = $checkEmailQuery->fetch(PDO::FETCH_ASSOC);

            if ($checkEmail !== false && $checkEmail['id'] > 0) {
                throw new Exception('Taki email już istnieje!');
            }

            $connection->beginTransaction();

            $createUserQuery = $connection->prepare('
            INSERT INTO `user` (`name`, surname, email, password)
            VALUES (:name, :surname, :email, :password)
        ');

            $createUserQuery->bindParam(':name', $name, PDO::PARAM_STR);
            $createUserQuery->bindParam(':surname', $surname, PDO::PARAM_STR);
            $createUserQuery->bindParam(':email', $email, PDO::PARAM_STR);
            $createUserQuery->bindParam(':password', $password, PDO::PARAM_STR);

            $createUserQuery->execute();

            $userId = $connection->lastInsertId();

            $userRole = User::ROLE_USER;

            $roleQuery = $connection->prepare('INSERT INTO user_group (user_id, group_id) VALUES (:userId, :groupId)');
            $roleQuery->bindParam(':userId', $userId, PDO::PARAM_INT);
            $roleQuery->bindParam(':groupId', $userRole, PDO::PARAM_INT);
            $roleQuery->execute();

            $logQuery = $connection->prepare('INSERT INTO user_register_log (user_id) VALUES (:userId)');
            $logQuery->bindParam(':userId', $userId, PDO::PARAM_INT);
            $logQuery->execute();

            $connection->commit();
        } catch (Exception $ex) {
            if ($connection->inTransaction()) {
                $connection->rollBack();
            }

            throw $ex;
        }
    }

    /**
     * @param int $userId
     */
    public function addUserLoginLog(int $userId): void
    {
        $insertLogQuery = $this->database->connect()->prepare('INSERT INTO user_login_log (user_id) VALUES (:userId)');
        $insertLogQuery->bindParam(':userId', $userId, PDO::PARAM_INT);
        $insertLogQuery->execute();
    }

    /**
     * @param int $id
     * @return User
     */
    public function getUserById(int $id): User
    {
        try {
            $stmt = $this->database->connect()->prepare('
                SELECT 
                u.id,
                u.name,
                u.surname,
                u.email,
                u.password,
                ug.group_id 
                FROM `user` u
                LEFT JOIN user_group ug ON ug.user_id = u.id 
                WHERE u.id = :id
            ');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            return new User($user['id'], $user['name'], $user['surname'], $user['email'], $user['password'], $user['group_id']);
        } catch (PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $userQuery = $this->database->connect()->prepare('
            SELECT
            id,
            name,
            surname,
            email
            FROM
            `user`
        ');
        $userQuery->execute();

        return $userQuery->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM `user` WHERE id = :id;');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (PDOException $e) {
            die();
        }
    }
}