<?php

/**
 * Class File
 */
class File
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $extension;

    /** @var int */
    private $userId;

    /**
     * File constructor.
     * @param int $id
     * @param string $name
     * @param string $extension
     * @param int $userId
     */
    public function __construct(int $id, string $name, string $extension, int $userId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->extension = $extension;
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}