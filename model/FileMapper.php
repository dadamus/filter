<?php

require_once __DIR__ . '/File.php';

/**
 * Class FileMapper
 */
class FileMapper
{
    /**
     * @var Database
     */
    private $database;

    /**
     * UserMapper constructor.
     */
    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * @param string $fileName
     * @param string $extension
     * @param int $userId
     * @return int
     * @throws Exception
     */
    public function save(string $fileName, string $extension, int $userId): int
    {
        $db = $this->database->connect();

        $db->beginTransaction();

        try {
            $insertFile = $db->prepare('
            INSERT INTO `file` (`name`, `extension`, `user_id`) VALUES (:name, :extension, :userId)
        ');
            $insertFile->bindParam(':name', $fileName, PDO::PARAM_STR);
            $insertFile->bindParam(':extension', $extension, PDO::PARAM_STR);
            $insertFile->bindParam(':userId', $userId, PDO::PARAM_INT);

            $insertFile->execute();

            $fileId = $db->lastInsertId();
        } catch (\Exception $ex) {
            if ($db->inTransaction()) {
                $db->rollBack();
            }
            throw $ex;
        }

        $db->commit();

        return $fileId;
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getByUserId(int $userId): array
    {
        $findFilesQuery = $this->database->connect()->prepare('
            SELECT
            *
            FROM
            `file`
            WHERE
            user_id = :userId
            AND deleted_at IS NULL
        ');
        $findFilesQuery->bindParam(':userId', $userId, PDO::PARAM_INT);
        $findFilesQuery->execute();

        $fileList = [];

        while ($data = $findFilesQuery->fetch()) {
            $fileList[] = new File(
                $data['id'],
                $data['name'],
                $data['extension'],
                $data['user_id']
            );
        }

        return $fileList;
    }

    /**
     * @param int $fileId
     * @param int $userId
     */
    public function delete(int $fileId, int $userId): void
    {
        $currentTime = date('Y-m-d H:i:s');

        $deleteFileQuery = $this->database->connect()->prepare('UPDATE `file` SET deleted_at = :date WHERE id = :id AND user_id = :userId');
        $deleteFileQuery->bindParam(':date', $currentTime, PDO::PARAM_STR);
        $deleteFileQuery->bindParam(':id', $fileId, PDO::PARAM_INT);
        $deleteFileQuery->bindParam(':userId', $userId, PDO::PARAM_INT);
        $deleteFileQuery->execute();
    }
}