<?php

require_once __DIR__ . '/Parameters.php';

class Database
{
    private $servername;
    private $username;
    private $password;
    private $database;


    public function __construct()
    {
        $this->servername = SERVERNAME;
        $this->username = USERNAME;
        $this->password = PASSWORD;
        $this->database = DBNAME;
    }

    public function connect()
    {
        return new PDO("mysql:host=$this->servername;dbname=$this->database", $this->username, $this->password);

    }
}

