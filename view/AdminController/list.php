<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__) . '/head.html'); ?>

<body>
<?php include(dirname(__DIR__) . '/navbar.html'); ?>

<div class="container">
    <div class="row">
        <h1 class="col-12">HOMEPAGE</h1>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-10">
                    <h4>Lista plików:</h4>
                </div>
                <div class="col-lg-2" style="text-align: right">
                    <a class="btn btn-info" href="?page=admin">Powrót</a>
                </div>
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Nazwa</th>
                    <th>Akcje</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($files as $file): ?>
                    <tr>
                        <td>
                            <a target="_blank"
                               href="/files/<?= $userId ?>/<?= $file->getId() ?>.<?= $file->getExtension() ?>">
                                <?= $file->getName() ?>
                            </a>
                        </td>
                        <td>
                            <a href="?page=admin_delete&user_id=<?= $userId ?>&file_id=<?= $file->getId() ?>">Usuń</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>