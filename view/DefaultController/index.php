<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__) . '/head.html'); ?>

<body>
<?php include(dirname(__DIR__) . '/navbar.html'); ?>

<div class="container">
    <div class="row">
        <h1 class="col-12">HOMEPAGE</h1>
    </div>

    <div class="row">
        <?php if (!$isAuthorized): ?>
            <div class="col-lg-12">
                <h3>Najpierw zaloguj się!</h3>
            </div>
        <?php else: ?>
            <div class="col-lg-12">
                <h4>Dodaj nowy plik:</h4>
                <form method="post" action="?page=upload" enctype="multipart/form-data">
                    <input type="file" name="to_upload" placeholder="Wybierz plik..."/>
                    <button type="submit">Wyślij</button>
                </form>
            </div>
            <div class="col-lg-12">
                <h4>Lista plików:</h4>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nazwa</th>
                        <th>Akcje</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($files as $file): ?>
                        <tr>
                            <td>
                                <a target="_blank" href="/files/<?= $userId ?>/<?= $file->getId() ?>.<?= $file->getExtension() ?>">
                                <?= $file->getName() ?>
                                </a>
                            </td>
                            <td>
                                <a href="?page=delete&file_id=<?= $file->getId() ?>">Usuń</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>

</body>
</html>